const express = require('express');
const axios = require('axios');
const config = require('config');
const Payment = require('../../models/payments.model');
const Order = require('../../../orders/models/orders.model');

const { removeEmptyKeyFrom, createSearch } = require('../../../../utils/db.util');

const router = express.Router();

const resourceUrl = config.get('API.ORDERS');

// get payment
router.get('/payments', async (req, res, next) => {
  try {
    const { $sort = 'createdAt', $limit = 20, $skip = 0, $search = {} } = req.query;
    const pagination = {
      limit: parseInt($limit, 10),
      skip: parseInt($skip, 10),
    };
    const queries = removeEmptyKeyFrom(req.query);
    const cleanedQueries = {
      ...createSearch({ params: $search, sql: false }),
      ...queries,
    };
    const total = await Payment.countDocuments({ ...cleanedQueries });
    const payments = await Payment.find({ ...cleanedQueries }, null, {
      sort: $sort,
      ...pagination,
    });

    return res.status(200).json({ total, ...pagination, data: payments });
  } catch (err) {
    return next(err);
  }
});

// create payment
router.post('/payments', async (req, res, next) => {
  try {
    const { orderId } = req.body;
    const payment = new Payment({ orderId });
    await payment.save();
    return res.status(201).json(payment);
  } catch (err) {
    return next(err);
  }
});

// patch payment
router.patch('/payments', async (req, res, next) => {
  try {
    const { paymentStatus } = req.body;
    const { orderId } = req.query;
    const payment = await Payment.findOneAndUpdate(orderId, { $set: paymentStatus }, { new: true });
    if (payment && paymentStatus === 'ACCEPTED') {
      // const orderStatus = await axios.patch(`${resourceUrl}/orders/${orderId}`, {
      //   status: 'CONFIRMED',
      // });
      const orderStatus = await Order.findOneAndUpdate({ _id: orderId }, { status: 'CONFIRMED' });
      await Payment.findOneAndUpdate(orderId, { $set: { deliveryStatus: 'DELIVERING' } });
      setTimeout(async () => {
        await Payment.findOneAndUpdate(orderId, { $set: { deliveryStatus: 'DELIVERED' } });
      }, 30000);
      return res.status(200).json(orderStatus);
    }
    if (payment && paymentStatus === 'DECLINED') {
      // const orderStatus = await axios.patch(`${resourceUrl}/orders/${orderId}`, {
      //   status: 'CANCELED',
      // });
      const orderStatus = await Order.findOneAndUpdate({ _id: orderId }, { status: 'DECLINED' });
      return res.status(200).json(orderStatus);
    }
    return res.sendStatus(200);
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
