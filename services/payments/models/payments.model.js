const mongoose = require('mongoose');

const PaymentSchema = new mongoose.Schema(
  {
    orderId: mongoose.Schema.Types.ObjectId,
    paymentStatus: {
      enum: ['PENDING', 'ACCEPTED', 'DECLINED'],
      type: String,
      default: 'PENDING',
    },
    deliveryStatus: {
      enum: ['PENDING', 'DELIVERING', 'DELIVERED'],
      type: String,
      default: 'PENDING',
    },
  },
  {
    timestamps: true,
    autoIndex: false,
  }
);

module.exports = mongoose.model('payments', PaymentSchema);
