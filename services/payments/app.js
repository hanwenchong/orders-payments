const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const errorhandler = require('errorhandler');
const helmet = require('helmet');
const useragent = require('express-useragent');
const mongoose = require('mongoose');

const { IS_PROD } = require('../../config');
const api = require('./routes/api');

const app = express();

app.use(helmet());
app.use(cors());
app.use(useragent.express());

// for logging
app.use(logger('dev'));
// parse request body to be readable json format
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

if (!IS_PROD) {
  app.use(errorhandler());
  mongoose.set('debug', true);
}

app.use(api);

if (!IS_PROD) {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      errors: {
        message: err.message,
        error: err,
      },
    });
    console.log(err.stack);
  });
}

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    errors: {
      message: err.message,
      error: {},
    },
  });
});

module.exports = app;
