/* eslint-disable max-len */
const mongoose = require('mongoose');
const { DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_HOST } = require('../../config');

module.exports = () => {
  const connectionString = `mongodb+srv://${DATABASE_USERNAME}:${DATABASE_PASSWORD}@${DATABASE_HOST}`;

  mongoose.connect(connectionString, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
  });

  const db = mongoose.connection;
  db.once('open', () => console.log('Connected to database.'));
  db.on('error', err => console.log(`Mongoose default connection has occured ${err} error.`));
  db.on('disconnected', () => console.log('Mongoose default connection is disconnected.'));

  process.on('SIGINT', () =>
    db.close(() => {
      console.log('Mongoose default connection is disconnected due to application termination.');
      process.exit(0);
    })
  );
};
