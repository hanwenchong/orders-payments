const config = require('../../config');
const app = require('./app');

const { APP_PAYMENTS } = config;

require('./mongo.db')();

app.listen(APP_PAYMENTS, () => console.log(`LISTENING ON PORT ${APP_PAYMENTS}`));
