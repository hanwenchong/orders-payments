const config = require('../../config');
const app = require('./app');

const { APP_ORDERS } = config;

require('./mongo.db')();

app.listen(APP_ORDERS, () => console.log(`LISTENING ON PORT ${APP_ORDERS}`));
