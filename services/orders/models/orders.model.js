const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const OrderSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      unique: true,
      required: true,
      uniqueCaseInsensitive: true,
    },
    status: {
      enum: ['CREATED', 'CONFIRMED', 'DELIVERED', 'CANCELED'],
      type: String,
      default: 'CREATED',
    },
  },
  {
    timestamps: true,
    autoIndex: false,
  }
);

OrderSchema.plugin(uniqueValidator);

module.exports = mongoose.model('orders', OrderSchema);
