const express = require('express');
const axios = require('axios');
const config = require('config');
const Order = require('../../models/orders.model');
const Payment = require('../../../payments/models/payments.model');

const { removeEmptyKeyFrom, createSearch } = require('../../../../utils/db.util');

const router = express.Router();

const resourceUrl = config.get('API.PAYMENTS');

// get all orders
router.get('/orders', async (req, res, next) => {
  try {
    const { $sort = 'createdAt', $limit = 20, $skip = 0, $search = {} } = req.query;
    const pagination = {
      limit: parseInt($limit, 10),
      skip: parseInt($skip, 10),
    };
    const queries = removeEmptyKeyFrom(req.query);
    const cleanedQueries = {
      ...createSearch({ params: $search, sql: false }),
      ...queries,
    };
    const total = await Order.countDocuments({ ...cleanedQueries });
    const orders = await Order.find({ ...cleanedQueries }, null, {
      sort: $sort,
      ...pagination,
    });

    return res.status(200).json({ total, ...pagination, data: orders });
  } catch (err) {
    return next(err);
  }
});

// get order by id
router.get('/ordders/:_id', async (req, res, next) => {
  try {
    const order = await Order.findById(req.params);
    if (order) {
      return res.status(200).json(order);
    }
    return res.status(404).json({
      message: 'Order does not exist.',
    });
  } catch (err) {
    return next(err);
  }
});

// create order
router.post('/orders', async (req, res, next) => {
  try {
    const { name } = req.body;
    const order = new Order({ name });
    await order.save();
    // await axios.post(`${resourceUrl}/payments`, { orderId: order._id });
    await Payment.create({ orderId: order._id });
    return res.status(201).json(order);
  } catch (err) {
    return next(err);
  }
});

// patch order
router.patch('/orders/:_id', async (req, res, next) => {
  try {
    const { body } = req;
    const { _id } = req.params;

    const order = await Order.findOneAndUpdate({ _id }, { $set: body }, { new: true });
    return res.status(200).json(order);
  } catch (err) {
    return next(err);
  }
});

// accept order
router.patch('/orders/confirm/:_id', async (req, res, next) => {
  try {
    const { _id } = req.params;
    const order = await Order.findOneAndUpdate(
      { _id },
      { $set: { status: 'CONFIRMED' } },
      { new: true }
    );
    if (order) {
      // await axios.patch(`${resourceUrl}/payments?orderId=${orderId}`, {
      //   paymentStatus: 'ACCEPTED',
      // });
      await Payment.findOneAndUpdate({ orderId: _id }, { $set: { paymentStatus: 'ACCEPTED' } });
    }
    return res.status(200).json(order);
  } catch (err) {
    return next(err);
  }
});

// cancel order
router.patch('/orders/cancel/:_id', async (req, res, next) => {
  try {
    const { _id } = req.params;
    const order = await Order.findOneAndUpdate(
      { _id },
      { $set: { status: 'CANCELED' } },
      { new: true }
    );
    if (order) {
      // await axios.patch(`${resourceUrl}/payments?orderId=${orderId}`, {
      //   paymentStatus: 'DECLINED',
      // });
      await Payment.findOneAndUpdate({ orderId: _id }, { $set: { paymentStatus: 'DECLINED' } });
    }
    return res.status(200).json(order);
  } catch (err) {
    return next(err);
  }
});

// check order status
router.get('/orders/status/:_id', async (req, res, next) => {
  try {
    const { _id } = req.params;
    const order = await Order.findById(_id, null, { select: 'status' });
    return res.status(200).json(order);
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
