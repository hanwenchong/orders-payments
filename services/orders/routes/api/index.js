const express = require('express');

const router = express.Router();

router.use(require('./order.route'));

module.exports = router;
