# API

Backend services for orders and payments.

# Setup

1.  Download git repository: manually ("clone" button above) or through command as below:

- `git clone https://hanwenchong@bitbucket.org/hanwenchong/orders-payments.git`

2.  Installation: `npm install`
3.  Run App: `npm start`
4.  To run test: `npm run test`

# Base URL

| URL                                   | Services    |
| ------------------------------------- | ----------- |
| http://localhost:3030                 | orders      |
| http://localhost:3000                 | payments    |


# Stack

- express.js

# Database

- mongo
