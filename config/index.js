require('dotenv').config();
const config = require('config');

const APP = config.get('APP');
const DATABASE = config.get('DATABASE');
const SECRET = config.get('SECRET');

const { APP_ORDERS, APP_PAYMENTS, NODE_ENV } = process.env;

const IS_PROD = NODE_ENV === 'production';

const sharedConfig = {
  APP_ORDERS: APP.ORDERS || APP_ORDERS,
  APP_PAYMENTS: APP.PAYMENTS || APP_PAYMENTS,
  NODE_ENV,
  IS_PROD,
  DATABASE_USERNAME: DATABASE.USERNAME,
  DATABASE_PASSWORD: DATABASE.PASSWORD,
  DATABASE_HOST: DATABASE.HOST,
  SECRET,
};

module.exports = sharedConfig;
