exports.removeEmptyKeyFrom = obj => {
  const format = new RegExp(/[!@#$%^&*(),.?":{}|<>]/g);
  const newObj = {};

  Object.keys(obj).forEach(key => {
    if (!format.test(key) && obj[key]) {
      newObj[key] = obj[key];
    }
  });

  return newObj;
};

exports.createSearch = ({ Op, params = {}, sql = true }) => {
  const filters = {};

  Object.keys(params).map(k => {
    filters[k] = new RegExp(params[k], 'i');
    return filters;
  });
  return Object.keys(filters).length !== 0 ? filters : null;
};
