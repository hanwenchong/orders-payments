const request = require('supertest');
const { expect } = require('chai');
const mongoose = require('mongoose');
const config = require('config');
const Order = require('../services/orders/models/orders.model');

const app = require('../services/orders/app');

require('../services/orders/mongo.db')();

describe('CRUD /orders', function() {
  let successOrder;
  let failedOrder;
  before(async function() {
    try {
      await mongoose.connection.dropCollection('orders', function(err, result) {
        if (err) {
          console.log('failed to drop orders.');
        }
        console.log('drop successful.');
      });
      successOrder = await Order.create({ name: 'successful order' });
      failedOrder = await Order.create({ name: 'failed order' });
    } catch (error) {
      console.error(error);
    }
  });
  describe('POST /orders', function() {
    it('201 Created', function(done) {
      request(app)
        .post('/orders')
        .set('Accept', 'application/json')
        .send({
          name: 'first order',
        })
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.property('statusCode');
          expect(res).to.have.property('body');
          expect(res).to.have.property('type');
          const { statusCode, body, type } = res;
          expect(statusCode).to.equal(201);
          expect(body).to.have.property('status');
          done();
        });
    });
  });
  describe('GET /orders', function() {
    it('200 Ok', function(done) {
      request(app)
        .get('/orders')
        .set('Accept', 'application/json')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.property('statusCode');
          expect(res).to.have.property('body');
          expect(res).to.have.property('type');
          const { statusCode, body, type } = res;
          expect(statusCode).to.equal(200);
          expect(body.data).to.be.an('array');
          done();
        });
    });
  });
  describe('PATCH confirm order', function() {
    it('200 Ok', function(done) {
      request(app)
        .patch(`/orders/confirm/${successOrder._id}`)
        .set('Accept', 'application/json')
        .end((err, res) => {
          expect(res).to.have.property('statusCode');
          expect(res).to.have.property('body');
          expect(res).to.have.property('type');
          const { statusCode, body, type } = res;
          expect(statusCode).to.equal(200);
          expect(body.status).to.equal('CONFIRMED');
          done();
        });
    });
  });
  describe('PATCH cancel order', function() {
    it('200 Ok', function(done) {
      console.log(failedOrder);
      request(app)
        .patch(`/orders/cancel/${failedOrder._id}`)
        .set('Accept', 'application/json')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.property('statusCode');
          expect(res).to.have.property('body');
          expect(res).to.have.property('type');
          const { statusCode, body, type } = res;
          expect(statusCode).to.equal(200);
          expect(body.status).to.equal('CANCELED');
          done();
        });
    });
  });
});
