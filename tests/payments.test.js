const request = require('supertest');
const { expect } = require('chai');
const mongoose = require('mongoose');
const config = require('config');
const Payment = require('../services/payments/models/payments.model');

const app = require('../services/payments/app');

require('../services/payments/mongo.db')();

describe('CRUD /payments', function() {
  describe('GET /payments', function() {
    it('200 Ok', function(done) {
      request(app)
        .get('/payments')
        .set('Accept', 'application/json')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.property('statusCode');
          expect(res).to.have.property('body');
          expect(res).to.have.property('type');
          const { statusCode, body, type } = res;
          expect(statusCode).to.equal(200);
          expect(body.data).to.be.an('array');
          done();
        });
    });
  });
});
